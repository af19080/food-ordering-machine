import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton gyozaButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JPanel root;
    private JLabel totalPrice;
    private int price = 0;

    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        String t = textPane1.getText();
        if(confirmation == 0){
            JOptionPane.showMessageDialog(null, "Thank you for ordering "+ food +"! It will be served as soon as possible.");
            textPane1.setText(t + "\n" + food);
            switch (food) {
                case "Tempura":
                    price += 100;
                    break;
                case "Karaage":
                    price += 100;
                    break;
                case "Gyoza":
                    price += 100;
                    break;
                case "Udon":
                    price += 100;
                    break;
                case "Yakisoba":
                    price += 100;
                    break;
                case "Ramen":
                    price += 100;
                    break;
            }
        }
        totalPrice.setText(price + " yen");
    }
    public FoodGUI() {
        totalPrice.setText(price + " yen");

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkOutConfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(checkOutConfirmation == 0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ price +" yen.");
                    price = 0;
                    totalPrice.setText(price + " yen");
                    textPane1.setText(null);
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
